# Jupiter server

Local environment for jupyter notebooks.

## Usage

```bash
docker compose up --build -d
```

## Access

- Jupyter: [http://localhost:8888](http://localhost:8888)

## Tools

set of tool to inser and extract data from the database

1 - Add stock to the database 

```bash
python script.py --name "Company Name" --symbol "SYM" --tags tag1 tag2 tag3
```