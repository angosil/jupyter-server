import argparse
import sqlite3

# Set up the argument parser
parser = argparse.ArgumentParser(description='Add stock data to a SQL table.')
parser.add_argument('--name', required=True, help='Name of the company')
parser.add_argument('--symbol', required=True, help='Symbol of the stock')
parser.add_argument('--tags', nargs='+', help='Set of tags')

# Parse the arguments
args = parser.parse_args()

# Connect to the SQLite database
conn = sqlite3.connect('stocks.db')

# Create a cursor object
c = conn.cursor()

# Create table if it doesn't exist
c.execute('''
    CREATE TABLE IF NOT EXISTS stocks
    (name text, symbol text, tags text)
''')

# Insert the provided data into the table
c.execute('''
    INSERT INTO stocks VALUES (?, ?, ?)
''', (args.name, args.symbol, ' '.join(args.tags)))

# Commit the changes and close the connection
conn.commit()
conn.close()
